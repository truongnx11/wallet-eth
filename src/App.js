import {useEffect, useState} from "react";
import {Provider} from "react-redux";
import "./App.css";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import {AppContext} from "libs/contextLib";
import AppRouter from "./routes";
import store from "./redux/store";


function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    // check authen o day !
  }, []);

  return (
    <AppContext.Provider
      value={{
        isAuthenticated,
        setIsAuthenticated,
      }}
    >
      <Provider store={store()}>
        <AppRouter />
        <ToastContainer/>
      </Provider>
    </AppContext.Provider>
  );
}

export default App;
