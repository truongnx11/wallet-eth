import * as bip39 from "bip39";
import * as storage from "../utils/storage";
import {STORAGE_KEYS} from "../constants";
import {decrypt, encrypt} from "./security";

const generateMnemonic = () => {
  const mnemonic = bip39.generateMnemonic();
  return mnemonic;
};

const saveMnemonic = (mnemonic, passKey) => {
  const hashedMnemonic = encrypt(mnemonic, passKey);
  storage.save(STORAGE_KEYS.MNEMONIC, hashedMnemonic);
};

const getMnemonic = (passKey) => {
  const hashedMnemonic = storage.find(STORAGE_KEYS.MNEMONIC);
  const mnemonic = decrypt(hashedMnemonic, passKey);
  return mnemonic;
};

const encryptAccounts = (accounts, passKey) => {
  const accountString = JSON.stringify(accounts);
  const encryptedAccounts = encrypt(accountString, passKey);
  return encryptedAccounts;
};

const decryptAccounts = (encAccounts, passKey) => {
  const decryptedString = decrypt(encAccounts, passKey);
  const accounts = JSON.parse(decryptedString);
  return accounts;
};

const getAccounts = (passKey) => {
  const encAccounts = storage.find(STORAGE_KEYS.ACCOUNTS);
  const accounts = decryptAccounts(encAccounts, passKey);
  return accounts || [];
};

const saveAccounts = (accounts = [], passKey) => {
  if (!Array.isArray(accounts)) throw new Error("Accounts must be an array");

  const encryptedAccounts = encryptAccounts(accounts, passKey);
  storage.save(STORAGE_KEYS.ACCOUNTS, encryptedAccounts);
};

const saveAccount = (account, passKey) => {
  const accounts = getAccounts(passKey);
  if (!Array.isArray(accounts)) throw new Error();

  accounts.push(account);
  const encryptedAccounts = encryptAccounts(accounts, passKey);

  storage.save(STORAGE_KEYS.ACCOUNTS, encryptedAccounts);
  return accounts.length - 1;
};

const findAccount = (condition) => {
  // TODO: query account here
};

const getTotalAccounts = (passKey) => {
  const accounts = getAccounts(passKey);
  if (!Array.isArray(accounts)) throw new Error();
  return accounts.length;
}

const isKeystoreEmpty = () => {
  const mnemonic = storage.find(STORAGE_KEYS.MNEMONIC);
  const accounts = storage.find(STORAGE_KEYS.ACCOUNTS);
  return !mnemonic || !accounts;
};

export {
  generateMnemonic,
  saveMnemonic,
  getMnemonic,
  saveAccounts,
  saveAccount,
  findAccount,
  getAccounts,
  getTotalAccounts,
  isKeystoreEmpty,
};
