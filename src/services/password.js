import {comparePassword, hashPassword} from "./security";
import * as storage from "../utils/storage";
import {STORAGE_KEYS} from "../constants";

const verifyCreatingPassword = (password, confirmPassword) => {
  let errors = {};
  if (!password) errors.password = "Password is required";
  if (!confirmPassword) errors.confirmPassword = "Confirm password is required";
  if (password !== confirmPassword)
    errors.confirmPassword = "Confirm password is not match";
  // TODO: check complexity of password

  if (Object.keys(errors).length) return errors;

  return null;
};

const createPassword = (plainText) => {
  const hashedPassword = hashPassword(plainText);
  storage.save(STORAGE_KEYS.PASSWORD, hashedPassword);
};

const verifyPassword = (plainTextPassword) => {
  const hashedPassword = storage.find(STORAGE_KEYS.PASSWORD);
  try {
    const result = comparePassword(plainTextPassword, hashedPassword);
    return result;
  } catch (e) {
    return false;
  }
};

export {verifyCreatingPassword, createPassword, verifyPassword};
