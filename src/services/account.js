import {ethers} from "ethers";
import * as storage from "../utils/storage";
import {STORAGE_KEYS} from "../constants";
import {DERIVATION_PATH} from "configs";

const setActiveAccountIndex = (index) => {
  storage.save(STORAGE_KEYS.ACTIVE_ACCOUNT_INDEX, index);
};

const getActiveAccountIndex = () => {
  return storage.find(STORAGE_KEYS.ACTIVE_ACCOUNT_INDEX);
};

const createAccount = (mnemonic, index) => {
  const wallet = ethers.Wallet.fromMnemonic(
    mnemonic,
    `${DERIVATION_PATH}/${index}`
  );
  return wallet;
};

export {setActiveAccountIndex, getActiveAccountIndex, createAccount};
