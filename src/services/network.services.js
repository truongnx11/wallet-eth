import axios from "axios";

class TransactionService {
  // Jobs
  static listTransaction({ baseUrl, enpoint }) {
    return axios.get(`${baseUrl}/api`, { params: { ...enpoint } });
  }
}
export default TransactionService;
