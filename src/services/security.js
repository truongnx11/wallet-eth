import bcrypt from "bcryptjs";
// import {COOKIE_LOGIN_LABEL} from "configs";
import CryptoJS from "crypto-js";
import sha256 from "crypto-js/sha256";
import {ERROR_CODE} from "errors/code";
import CustomError from "errors/CustomError";
// import {getCookie} from "utils/cookie";

// import {SECRET_KEY} from "../configs";

const saltRounds = 10;

// const getSecretKey = () => {
//   const passKey = getCookie(COOKIE_LOGIN_LABEL);
//   if (!passKey) throw new Error("PassKey is null");
// };

const encrypt = (text, passKey) => {
  const cipherText = CryptoJS.AES.encrypt(text, passKey).toString();
  return cipherText;
};

const decrypt = (cipherText, passKey) => {
  // const passKey = getSecretKey();
  try {
    const bytes = CryptoJS.AES.decrypt(cipherText, passKey);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    if (!originalText) {
      console.log('Keystore đã bị thay đổi từ dev tools');
      // TODO: handle this
    }
    return originalText;
  } catch (error) {
    throw new CustomError(ERROR_CODE.WRONG_PASS_KEY);
  }
};

const hashPassword = (plainText) => {
  const salt = bcrypt.genSaltSync(saltRounds);
  const hash = bcrypt.hashSync(plainText, salt);
  return hash;
};

const comparePassword = (password, hashedPassword) => {
  return bcrypt.compareSync(password, hashedPassword);
};

const hashString = (plainText) => {
  return sha256(plainText).toString();
};

export {hashPassword, comparePassword, encrypt, decrypt, hashString};
