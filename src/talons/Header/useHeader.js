import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { handleLogoutUser } from "redux/auth/actions";
import { changeNetwork } from "../../redux/network/actions";

const useHeader = () => {
  const dispatch = useDispatch();

  const handleChangeNetwork = useCallback((e) => {
    dispatch(changeNetwork(e));
    localStorage.setItem("network", e);
  }, []);

  const handleLogout = useCallback(() => {
    dispatch(handleLogoutUser())
  }, []);

  return { handleChangeNetwork, handleLogout };
};

export default useHeader;
