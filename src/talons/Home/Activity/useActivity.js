import {
  convertNumToMonth,
  iconRecieve,
  IconSend,
  iconUpdate,
} from "utils/constants";
import { useSelector } from "react-redux";
import Web3 from "web3";

const useActivity = () => {
  const account = useSelector((state) => state.account);

  const checkSendAccount = (fromAddress) => {
    return fromAddress === account.address.toLowerCase();
  };
  const weiToEth = (wei) => Web3.utils.fromWei(wei, "ether");

  const checkStatusItem = (item) => {
    if (item.contractAddress !== "") return "Contract Deployment";
    return checkSendAccount(item.from) ? "Send" : "Recieve";
  };

  const checkStatusIcon = (item) => {
    if (item.contractAddress !== "") return iconUpdate;
    return checkSendAccount(item.from) ? IconSend : iconRecieve;
  };

  const converTime = (item) => {
    var date = new Date(item.timeStamp * 1000);
    return `${convertNumToMonth(date.getMonth())} ${date.getDate()}`;
  };

  return {
    weiToEth,
    checkStatusItem,
    checkStatusIcon,
    converTime,
    checkSendAccount,
  };
};

export default useActivity;
