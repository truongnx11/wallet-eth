import { useCallback, useEffect, useState } from "react";
import { Ether, Gwei, maxGasForEthSend } from "utils/constants";
import BigNumber from "bignumber.js";
import SignerProvider from "ethjs-provider-signer";
import signIn from "ethjs-signer";
import { useDispatch, useSelector } from "react-redux";
import Web3 from "web3";
import Network from "constants/network";
import { getTransactions } from "redux/transactions/actions";
import { toast } from "react-toastify";
import { getListAssets } from "redux/assets/actions";
import { erc20Abi } from "utils/abi";

const web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");

const TABS = {
  ASSETS: "Assets",
  ACTIVITY: "Activity",
};

const useHome = () => {
  const dispatch = useDispatch();
  const account = useSelector((state) => state.account);
  const networkSelected = useSelector((state) => state.network.key);
  const [activeTab, setActiveTab] = useState(TABS.ASSETS);
  const [visible, setVisible] = useState(false);
  const listAssets = useSelector((state) => state.assets.assets);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [address, setAddress] = useState("");
  const [validate, setValidate] = useState(false);
  const [amount, setAmount] = useState(0);
  const [isCreating, setIsCreating] = useState(false);
  const [message, setMessage] = useState("");
  const [balance, setBalance] = useState(0);
  const [isSuccess, setIsSuccess] = useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
  };

  const deleteTransaction = useCallback(() => {
    setIsCreating(false);
    setAddress("");
    setAmount(0);
    setValidate(false);
    setIsSuccess(false);
  }, []);

  const changeTab = (tab) => {
    setActiveTab(tab);
  };

  const onChangeText = (e) => setAddress(e.target.value);

  useEffect(() => {
    if (address) {
      const valid = web3.utils.isAddress(address);
      valid ? setValidate(true) : setValidate(false);
    }
  }, [address]);

  useEffect(() => {
    account.address !== "" &&
      dispatch(
        getListAssets({ addressAccount: account.address, networkSelected })
      );
  }, [account.address, listAssets, networkSelected]);

  const sendTransactions = async () => {
    let tx;
    // const gasPrice = new BigNumber(0.00000002).times(Gwei);
    const gasPrice = 0.000000027802068995 * 1e18; // new BigNumber(10).times(Gwei);
    const sendAmount = new BigNumber(amount).times(Ether);
    const sendParams = {
      from: account.address,
      to: address,
      value: sendAmount,
      gasPrice,
      gas: maxGasForEthSend,
    };
    function sendTransactionPromise(params) {
      // eslint-disable-line no-inner-declarations
      return new Promise((resolve, reject) => {
        web3.eth.sendTransaction(params, (err, data) => {
          if (err !== null) return reject(err);
          setIsSuccess(data);
          toast("Transaction sent success!", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            type: "success",
          });
          return resolve(data);
        });
      });
    }
    tx = await sendTransactionPromise(sendParams).catch((err) => {
      console.log({ err });
      toast("Transaction send not success!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        type: "error",
      });
    });
  };

  const sendToken = async (tokenContractAddress, addressRecieve, amount) => {
    // const gasPrice = 0.000000049802068995 * 1e18; // new BigNumber(10).times(Gwei);
    const networkId = await web3.eth.net.getId();
    const sendAmount = new BigNumber(amount).times(Ether);
    try {
      const tokenContract = new web3.eth.Contract(
        erc20Abi,
        tokenContractAddress,
        { from: account.address }
      );
      const tx = tokenContract.methods.transfer(addressRecieve, sendAmount);
      const gas = await tx.estimateGas({ from: account.address });
      const gasPrice = await web3.eth.getGasPrice();
      const data = tx.encodeABI();
      const nonce = await web3.eth.getTransactionCount(account.address);
      const signedTx = await web3.eth.accounts.signTransaction(
        {
          to: tokenContract.options.address,
          data,
          gas,
          gasPrice,
          nonce,
          chainId: networkId,
        },
        account.privateKey
      );
      const receipt = await web3.eth.sendSignedTransaction(
        signedTx.rawTransaction
      );
      console.log({ receipt });
      toast.success("Transaction has been sent!");
      return { transactionHash: receipt.transactionHash };
    } catch (error) {
      toast.error("Transaction has been reverted");
      console.log({ error });
    }
  };

  const confirmTransaction = useCallback(() => {
    try {
      const msg = `Transaction created successfully. 
      Sending ${amount} from ...${account.address.slice(
        -5
      )} to ...${address.slice(-5)}`;
      setIsCreating(true);
      setMessage(msg);
      if (!web3.utils.isAddress(address)) {
        throw new Error("Destenation address invalid");
      }
    } catch (error) {
      setMessage("Can't create transaction!");
      // setMessage()
      console.log({ error });
    }
  }, [address, amount]);

  useEffect(() => {
    if (networkSelected && account.address !== "") {
      // Update Balance when Network changed
      const rpc = Network[networkSelected].rpc;
      const provider = new SignerProvider(rpc, {
        signTransaction: (rawTx, cb) =>
          cb(null, signIn.sign(rawTx, account.privateKey)),
        accounts: (cb) => cb(null, account.address),
      });
      web3.setProvider(provider);
      const fetchBalance = async () => {
        let balanceAddress = await web3.eth.getBalance(account.address);
        balanceAddress = Web3.utils.fromWei(balanceAddress, "ether");
        setBalance(balanceAddress);
      };
      fetchBalance().catch();
    }
  }, [networkSelected, account]);

  useEffect(() => {
    if (account.address && networkSelected) {
      dispatch(
        getTransactions({
          network: Network[networkSelected].api,
          address: account.address,
        })
      );
    }
  }, [account, networkSelected]);

  return {
    showModal,
    validate,
    confirmLoading,
    handleCancel,
    onChangeText,
    activeTab,
    visible,
    sendTransactions,
    changeTab,
    address,
    TABS,
    setAmount,
    amount,
    confirmTransaction,
    isCreating,
    message,
    balance,
    deleteTransaction,
    isSuccess,
    sendToken,
  };
};

export default useHome;
