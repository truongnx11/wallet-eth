import React, {useState} from "react";
import {Link, useHistory} from "react-router-dom";
import {Space, Typography, Input, Button, notification} from "antd";
import {EyeInvisibleOutlined, EyeTwoTone} from "@ant-design/icons";

import "./index.style.scss";
import {verifyPassword} from "services/password";

import {ROUTES} from "constants/routes";
import useLogin from "hooks/useLogin";

const {Title} = Typography;

const Login = () => {
  const history = useHistory();
  const [password, setPassword] = useState("");
  const [error, setError] = useState();

  const {saveLoginState} = useLogin();

  const handleLogin = () => {
    if (!password) {
      setError("Password is empty");
      return;
    }
    const satisfied = verifyPassword(password);
    if (!satisfied) {
      setError("Password not match");
      return;
    }
    saveLoginState(password);

    notification.success({
      message: "Login successfully",
    });
    history.push(ROUTES.HOME);
  };

  const handleChangePassword = (event) => {
    setPassword(event.target.value);
    setError(null);
  };

  return (
    <div className="login-wrapper">
      <Space direction="vertical" align="center" size="middle">
        <Title level={1}>Login</Title>
        <Typography>Enter your password to log in to your wallet</Typography>

        <div>
          <Input.Password
            placeholder="Enter your password"
            value={password}
            onChange={handleChangePassword}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            onPressEnter={handleLogin}
            status={!!error ? "error" : ""}
          />
          {error && <Typography className="error">{error}</Typography>}
        </div>
        <Button type="primary" onClick={handleLogin}>
          GO
        </Button>

        <Link to={ROUTES.RESTORE} className="forgot-pw">Forgot password?</Link>
      </Space>
    </div>
  );
};

export default Login;
