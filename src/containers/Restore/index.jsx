import React from "react";
import {Button, Typography} from "antd";
import "./index.style.scss";
import useRestoreWallet from "hooks/useRestoreWallet";

const {Title} = Typography;

const Restore = () => {
  const {renderForm, handleRestoreWallet} = useRestoreWallet();

  const handleImportWallet = () => {
    handleRestoreWallet();
  };

  return (
    <div className="restore-wrapper">
      <Title level={1}>Reset password</Title>
      <Typography>
        MetaMask does not keep a copy of your password. If you’re having trouble
        unlocking your account, you will need to reset your wallet. You can do
        this by providing the Secret Recovery Phrase you used when you set up
        your wallet.
      </Typography>
      <Typography>
        This action will delete your current wallet and Secret Recovery Phrase
        from this device, along with the list of accounts you’ve curated. After
        resetting with a Secret Recovery Phrase, you’ll see a list of accounts
        based on the Secret Recovery Phrase you use to reset. This new list will
        automatically include accounts that have a balance. You’ll also be able
        to re-add any other accounts created previously. Custom accounts that
        you’ve imported will need to be re-added, and any custom tokens you’ve
        added to an account will need to be re-added as well.
      </Typography>
      <Typography>
        Make sure you’re using the correct Secret Recovery Phrase before
        proceeding. You will not be able to undo this.
      </Typography>

      {renderForm()}

      <Button size="large" type="primary" onClick={handleImportWallet}>
        Reset
      </Button>
    </div>
  );
};

export default Restore;
