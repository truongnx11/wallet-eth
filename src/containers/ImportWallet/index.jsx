import React from "react";
import {Button, notification, Typography} from "antd";
import "./index.style.scss";
import useRestoreWallet from "hooks/useRestoreWallet";
import useBackHeader from "hooks/useBackHeader";

const {Title} = Typography;

const ImportWallet = () => {
  const {renderForm, handleRestoreWallet} = useRestoreWallet();
  const {renderBackHeader} = useBackHeader();

  const handleImportWallet = () => {
    handleRestoreWallet();
    
  };

  return (
    <div className="import-wrapper">
      {renderBackHeader()}
      <Title level={1}>Import a wallet with Secret Recovery Phrase</Title>
      <Typography>
        Only the first account on this wallet will auto load. After completing
        this process, to add additional accounts, click the drop down menu, then
        select Create Account.
      </Typography>

      {renderForm()}

      <Button size="large" type="primary" onClick={handleImportWallet}>
        Import
      </Button>
    </div>
  );
};

export default ImportWallet;
