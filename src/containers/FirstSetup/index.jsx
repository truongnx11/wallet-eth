import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {useHistory, useLocation} from "react-router-dom";
import {Button} from "antd";
import queryString from "query-string";
import {FIRST_SETUP_TYPE, WALLET_CREATION_STEP} from "../../constants";
import {ROUTES} from "../../constants/routes";
import CreateWallet from "containers/CreateWallet";
import './index.style.scss';
import ImportWallet from "containers/ImportWallet";

const FirstSetup = () => {
  const history = useHistory();
  const location = useLocation();
  const {accounts} = useSelector((state) => state.keystore);
  const {isAuthenticated, verifying} = useSelector((state) => state.auth);

  const [setupType, setSetupType] = useState(null);

  const handleFirstSetup = (type) => (e) => {
    const params = {
      type,
      step: WALLET_CREATION_STEP.CREATE_PASSWORD,
    };
    history.replace({
      search: queryString.stringify(params),
    });
  };

  useEffect(() => {
    const {search} = location;
    const {type} = queryString.parse(search);

    setSetupType(type);
  }, [location]);

  useEffect(() => {
    if (!verifying && accounts.length && !isAuthenticated) {
      history.push(ROUTES.LOGIN);
    }
  }, [verifying, accounts, isAuthenticated, history]);

  const renderComponent = () => {
    switch (setupType) {
      case FIRST_SETUP_TYPE.CREATE_WALLET:
        return <CreateWallet />;
      case FIRST_SETUP_TYPE.IMPORT_WALLET:
        return <ImportWallet />;
      default:
        return (
          <div className="select-type">
            <Button onClick={handleFirstSetup(FIRST_SETUP_TYPE.IMPORT_WALLET)}>
              Import Wallet
            </Button>
            <Button onClick={handleFirstSetup(FIRST_SETUP_TYPE.CREATE_WALLET)}>
              Create Wallet
            </Button>
          </div>
        );
    }
  };

  return <div className="wrapper">{renderComponent()}</div>;
};

export default FirstSetup;
