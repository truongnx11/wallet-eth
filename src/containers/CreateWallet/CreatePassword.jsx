import React from "react";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {Typography, Input, Space, Button, PageHeader} from "antd";
import {EyeInvisibleOutlined, EyeTwoTone} from "@ant-design/icons";
import queryString from "query-string";
import {createPassword} from "../../services/password";
import {FIRST_SETUP_TYPE, WALLET_CREATION_STEP} from "../../constants";
import "./styles.scss";
import {hashString} from "services/security";
import actions from "redux/actions";
import useCreatePassword from "hooks/useCreatePassword";
import useBackHeader from "hooks/useBackHeader";

const {Title} = Typography;

/**
 *
 * @param {*} param0 firstSetupType: CreateWallet or ImportWallet
 * @returns
 */
const CreatePassword = ({firstSetupType}) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const {
    password,
    confirmPassword,
    errors,
    verifyPassword,
    handleChangePassword,
    handleChangeConfirmPassword,
  } = useCreatePassword();

  const {renderBackHeader} = useBackHeader();

  const handleCreatePassword = () => {
    const satisfied = verifyPassword();
    if (!satisfied) return;

    createPassword(password);

    const passKey = hashString(password);
    dispatch(actions.auth.setPassKeyOnFirstSetup(passKey));

    let params;

    if (firstSetupType === FIRST_SETUP_TYPE.CREATE_WALLET) {
      params = {
        step: WALLET_CREATION_STEP.SECURE_WALLET_INSTRUCTION,
        type: FIRST_SETUP_TYPE.CREATE_WALLET,
      };
    }
    // else: Handle wallet importation
    history.replace({
      search: queryString.stringify(params),
    });
  };

  return (
    <div className="pw-creation-wrapper">
      {renderBackHeader()}
      <Title level={4}>Create your password</Title>
      <Space direction="vertical">
        <Input.Password
          placeholder="Enter password"
          value={password}
          onChange={handleChangePassword}
          status={!!errors?.password ? "error" : ""}
        />
        <Typography className="error">{errors?.password}</Typography>
        <Input.Password
          placeholder="Confirm password"
          value={confirmPassword}
          onChange={handleChangeConfirmPassword}
          iconRender={(visible) =>
            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
          }
          status={!!errors?.confirmPassword ? "error" : ""}
        />
        <Typography className="error">{errors?.confirmPassword}</Typography>
      </Space>
      <Button type="primary" className="btn" onClick={handleCreatePassword}>
        Create
      </Button>
    </div>
  );
};

export default CreatePassword;
