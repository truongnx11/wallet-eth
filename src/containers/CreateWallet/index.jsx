import React, {useEffect, useState} from "react";
import {useLocation, Redirect} from "react-router-dom";
import queryString from "query-string";
import CreatePassword from "containers/CreateWallet/CreatePassword";
import SecureWalletInstruction from "./SecureWalletInstruction";
import {ROUTES} from "../../constants/routes";
import {FIRST_SETUP_TYPE, WALLET_CREATION_STEP} from "../../constants";
import MnemonicView from "./MnemonicView";

const CreateWallet = () => {
  const location = useLocation();
  const [activeStep, setActiveStep] = useState(
    WALLET_CREATION_STEP.CREATE_PASSWORD
  );

  useEffect(() => {
    const {search} = location;
    const {step} = queryString.parse(search);

    setActiveStep(parseInt(step, 10));
  }, [location]);

  switch (activeStep) {
    case WALLET_CREATION_STEP.CREATE_PASSWORD:
      return <CreatePassword firstSetupType={FIRST_SETUP_TYPE.CREATE_WALLET} />;
    case WALLET_CREATION_STEP.SECURE_WALLET_INSTRUCTION:
      return <SecureWalletInstruction />;
    case WALLET_CREATION_STEP.SECRET_PHASE:
      return <MnemonicView />;
    default:
      return <Redirect to={ROUTES.FIRST_SETUP} />;
  }
};

export default CreateWallet;
