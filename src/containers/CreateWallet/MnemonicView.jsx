import React, {useEffect, useState} from "react";
import {Button, notification, PageHeader, Typography} from "antd";
import {useHistory, useLocation} from "react-router-dom";
import queryString from "query-string";
import "./styles.scss";
import {FIRST_SETUP_TYPE, WALLET_CREATION_STEP} from "../../constants";
import {ROUTES} from "constants/routes";
import {useDispatch} from "react-redux";
import actions from "redux/actions";
import {useSelector} from "react-redux";

const {Title} = Typography;

const MnemonicView = () => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();
  const {passKeyOnSetup} = useSelector((state) => state.auth);

  const [mnemonic, setMnemonic] = useState("");

  const handleReplaceStep = (step) => {
    const params = {
      step,
      type: FIRST_SETUP_TYPE.CREATE_WALLET,
    };

    history.replace({
      search: queryString.stringify(params),
    });
  };

  const handleBackToInstruction = () => {
    handleReplaceStep(WALLET_CREATION_STEP.SECURE_WALLET_INSTRUCTION);
  };

  const handleFinishing = () => {
    if (!mnemonic) {
      notification.open({
        message: "Create wallet error",
        description: "Create seed failed, please try again!",
      });
      handleBackToInstruction();
      return;
    }
    dispatch(actions.keystore.createWallet(mnemonic, passKeyOnSetup));

    history.push(ROUTES.CONGRATULATION);
  };

  useEffect(() => {
    const {step} = queryString.parse(location.search);
    if (!location.state?.mnemonic && step === WALLET_CREATION_STEP.SECRET_PHASE)
      history.push(ROUTES.FIRST_SETUP);
    else setMnemonic(location.state?.mnemonic);
  }, [location, history]);

  if (!passKeyOnSetup) {
    handleReplaceStep(WALLET_CREATION_STEP.CREATE_PASSWORD);
    return "";
  }

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={handleBackToInstruction}
        title="Metamask"
        subTitle="Back"
      />
      <Title level={3}>Secret Recovery Phrase</Title>
      <Typography>
        Your Secret Recovery Phrase makes it easy to back up and restore your
        account. WARNING: Never disclose your Secret Recovery Phrase. Anyone
        with this phrase can take your Ether forever.
      </Typography>

      <div className="seed-view">{mnemonic}</div>
      <Button type="primary" onClick={handleFinishing}>
        Next
      </Button>
    </div>
  );
};

export default MnemonicView;
