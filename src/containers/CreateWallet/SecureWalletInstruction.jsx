import React from "react";
import { useSelector } from "react-redux";
import {Button, PageHeader, Typography} from "antd";
import {useHistory} from "react-router-dom";
import queryString from "query-string";
import "./styles.scss";
import {FIRST_SETUP_TYPE, WALLET_CREATION_STEP} from "../../constants";
import {generateMnemonic} from "../../services/keystore";

const {Title} = Typography;

const SecureWalletInstruction = () => {
  const history = useHistory();
  const {passKeyOnSetup} = useSelector((state) => state.auth);

  const handleReplaceStep = (step) => {
    const params = {
      step,
      type: FIRST_SETUP_TYPE.CREATE_WALLET,
    };

    history.replace({
      search: queryString.stringify(params),
    });
  };

  const handleBackToCreateWallet = () => {
    handleReplaceStep(WALLET_CREATION_STEP.CREATE_PASSWORD);
  };

  const handleNextStep = () => {
    const mnemonic = generateMnemonic();
    const params = {
      step: WALLET_CREATION_STEP.SECRET_PHASE,
      type: FIRST_SETUP_TYPE.CREATE_WALLET,
    };

    history.replace({state: {mnemonic}, search: queryString.stringify(params)});
  };

  if (!passKeyOnSetup) {
    handleReplaceStep(WALLET_CREATION_STEP.CREATE_PASSWORD);
    return "";
  }

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={handleBackToCreateWallet}
        title="Metamask"
        subTitle="Back"
      />
      <Title level={3}>Secure Your wallet</Title>
      <Typography>
        Before getting started, watch this short video to learn about your
        Secret Recovery Phrase and how to keep your wallet safe.
      </Typography>
      <Button type="primary" onClick={handleNextStep}>
        Next
      </Button>
    </div>
  );
};

export default SecureWalletInstruction;
