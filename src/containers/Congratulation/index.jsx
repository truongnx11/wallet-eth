import React from "react";
import {Button, Space, Typography} from "antd";
import { useHistory } from "react-router-dom";
import { ROUTES } from "constants/routes";
import "./index.style.scss";

const {Title} = Typography;

const Congratulation = () => {
  const history = useHistory();

  const handleDone = () => {
    history.push(ROUTES.LOGIN);
  };

  return (
    <div>
      <Title level={3}>CONGRATULATION</Title>
      <Space direction="vertical">
        <div
          dangerouslySetInnerHTML={{
            __html: `You passed the test - keep your Secret Recovery Phrase safe, it's your
        responsibility!Tips on storing it safely </br>• Save a backup in multiple
      places. </br>• Never share the phrase with anyone. </br>• Be careful of phishing!
      MetaMask will never spontaneously ask for your Secret Recovery Phrase. </br>•
      If you need to back up your Secret Recovery Phrase again, you can find
      it in Settings -> Security. </br>• If you ever have questions or see
      something fishy, contact our support here. *MetaMask cannot recover your
      Secret Recovery Phrase. Learn more.`,
          }}
        />
        <Typography>Come to Login page</Typography>
        <Button type="primary" onClick={handleDone}>
          All Done
        </Button>
      </Space>
    </div>
  );
};

export default Congratulation;
