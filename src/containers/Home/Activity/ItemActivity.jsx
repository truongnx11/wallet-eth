import React, { useState } from "react";
import "./ItemActivity.scss";
import { Modal } from "antd";
import Network from "constants/network";
import useActivity from "talons/Home/Activity/useActivity";
import { useSelector } from "react-redux";
import { subText } from "utils/constants";

const ItemActivity = ({ item }) => {
  const networkSelected = useSelector((state) => state.network.key);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {
    checkStatusIcon,
    checkStatusItem,
    converTime,
    weiToEth,
    checkSendAccount,
  } = useActivity();

  const handleShowDetailItem = () => {
    setIsModalVisible(true);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const copyText = () => {
    // loading the content into our clipboard
    navigator.clipboard.writeText(item.hash);
  };
  console.log({ item });
  return (
    <>
      <div className="wrap-content-acitivity" onClick={handleShowDetailItem}>
        <div className="wrap-activity-left">
          <div className="icon-ac">{checkStatusIcon(item)}</div>
          <div className="left-content">
            <h5>{checkStatusItem(item)}</h5>
            <p>
              <span className="date-ac">{converTime(item)}</span>{" "}
              <span className="to-ac">
                {checkSendAccount(item.from)
                  ? `From: ${subText(item.from)}`
                  : `To: ${subText(item.to)}`}
              </span>
            </p>
          </div>
        </div>
        <div className="wrap-activity-right">
          <h5>
            {checkSendAccount(item.from) && "- "}
            {weiToEth(item.value)} ETH
          </h5>
          <h6>
            {checkSendAccount(item.from) && "- "}
            {weiToEth(item.value)} ETH
          </h6>
        </div>
      </div>
      <Modal
        title={checkStatusItem(item)}
        visible={isModalVisible}
        onOk={handleOk}
        footer={null}
        onCancel={handleCancel}
      >
        <div className="status-modal">
          <p className="status-text">Status</p>
          <a
            target={"_blank"}
            href={`${Network[networkSelected].tx_explorer}${item.hash}`}
          >
            View on block explorer
          </a>
        </div>
        <div className="confirm-status">
          <p className="confirm-text">Confirmed</p>
          <p onClick={copyText} className="copy-id">
            Copy Transaction ID
          </p>
        </div>

        <div className="status-from">
          <p>From</p>
          <p>To</p>
        </div>
        <div className="value-from">
          <p>{subText(item.from)}</p>
          <p>
            {item.contractAddress.length > 0
              ? "New Contract"
              : subText(item.to)}
          </p>
        </div>
        <h2>Transaction</h2>
        <div className="wrap-item">
          <p>Nounce</p>
          <p>{item.nonce}</p>
        </div>
        <div className="wrap-item">
          <p>Amount</p>
          <p className="eth-value">
            {checkSendAccount(item.from) && "- "}
            {weiToEth(item.value)} ETH
          </p>
        </div>
        <div className="wrap-item">
          <p>Gas Limit (Units)</p>
          <p>{item.gasUsed}</p>
        </div>
        <div className="wrap-item">
          <p>Gas Used (Units)</p>
          <p>{item.gasUsed}</p>
        </div>
        <div className="wrap-item">
          <p>Total Gas Fee</p>
          <p>{weiToEth(+item.gasPrice * +item.gasUsed + "").slice(0, 9)} ETH</p>
        </div>
        <div className="wrap-item">
          <p>Total</p>
          <p className="eth-value">
            {weiToEth(+item.value + +item.gasPrice * +item.gasUsed + "").slice(
              0,
              9
            )}{" "}
            ETH
          </p>
        </div>
      </Modal>
    </>
  );
};

export default ItemActivity;
