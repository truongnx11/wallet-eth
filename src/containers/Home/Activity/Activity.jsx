import React, { useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import "./Activity.scss";
import ItemActivity from "./ItemActivity";
import { loadMoreTransactions } from "redux/transactions/actions";
import Network from "constants/network";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const Activity = () => {
  const dispatch = useDispatch();
  const transactions = useSelector(
    (state) => state.transactions.listTransaction
  );
  
  const account = useSelector((state) => state.account);
  const networkSelected = useSelector((state) => state.network.key);
  const currentPage = useSelector((state) => state.transactions.page);
  const hasMore = useSelector((state) => state.transactions.canLoadMore);
  const loading = useSelector((state) => state.transactions.isLoading);

  const handleLoadMore = useCallback(() => {
    dispatch(
      loadMoreTransactions({
        page: currentPage + 1,
        network: Network[networkSelected].api,
        address: account.address,
      })
    );
  }, [currentPage, networkSelected, account]);

  const content = useMemo(() => {
    if (!transactions) {
      return null;
    } else if (!transactions.length) {
      return <div className="no-transaction">You have no transactions!</div>;
    } else {
      return transactions.map((item, index) => {
        return <ItemActivity item={item} key={index} />;
      });
    }
  }, [transactions]);

  return (
    <div>
      {content}
      <div className="bottom-content">
        {hasMore &&
          (loading ? (
            <Spin indicator={antIcon} size="large" />
          ) : (
            <button className="btnLoadMore" onClick={handleLoadMore}>View More</button>
          ))}
      </div>
    </div>
  );
};

export default React.memo(Activity);
