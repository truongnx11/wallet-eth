import Network from "constants/network";
import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { Modal, Button } from "antd";

const SendTransactionModal = (props) => {
  const networkSelected = useSelector((state) => state.network.key);

  const {
    visible,
    validate,
    confirmTransaction,
    deleteTransaction,
    balance,
    isSuccess,
    sendTransactions,
    message,
    confirmLoading,
    setAmount,
    handleCancel,
    amount,
    isCreating,
    address,
    onChangeText,
  } = props;

  const amountValidate = useMemo(
    () => amount > balance || amount <= 0,
    [amount, balance]
  );
  
  return (
    <Modal
      title="Send Transaction"
      visible={visible}
      confirmLoading={confirmLoading}
      onCancel={handleCancel}
      footer={null}
    >
      <h4>Address :</h4>
      <input
        disabled={isCreating}
        value={address}
        onChange={onChangeText}
        className="input-send"
        type="text"
        name="name"
        id=""
      />
      <div>
        {validate ? (
          <h5 className="valid">Address is valid!</h5>
        ) : (
          <h5 className="not-valid">Address is not valid!</h5>
        )}
      </div>
      {validate && (
        <div className="number-input">
          <input
            disabled={isCreating}
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
            className="input-send"
            type="number"
            name="name"
            id=""
          />
        </div>
      )}

      {validate &&
        (amountValidate ? (
          <h5 className="not-valid">Not enough!</h5>
        ) : (
          <h5 className="valid">Amount is valid</h5>
        ))}
      {isCreating ? (
        <div className="wrap-message">
          <h4 className="transaction-message">{message}</h4>
          <button onClick={deleteTransaction} className="btn-back">
            Back
          </button>
        </div>
      ) : (
        validate &&
        !amountValidate && (
          <Button onClick={confirmTransaction} type="primary">
            Create transaction
          </Button>
        )
      )}

      {validate && isCreating && !isSuccess && (
        <Button onClick={sendTransactions}>Send</Button>
      )}
      {isSuccess && (
        <a
          target={"_blank"}
          href={Network[networkSelected].tx_explorer + isSuccess}
        >
          See transaction
        </a>
      )}
    </Modal>
  );
};

export default SendTransactionModal;
