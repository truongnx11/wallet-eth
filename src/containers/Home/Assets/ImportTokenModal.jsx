import React, { useCallback, useState } from "react";
import { Modal } from "antd";
import Web3 from "web3";
import "./Assets.scss";
import { useDispatch, useSelector } from "react-redux";
import { addAssets } from "redux/assets/actions";

const web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");

const ImportTokenModal = ({ isModalVisible, handleCancel, handleOk }) => {
  const [tokenAddress, setTokenAddress] = useState("");
  const [tokenSymbol, setTokenSymbol] = useState("");
  const [firtInput, setFirstInput] = useState(true);
  const listAssets = useSelector((state) => state.assets.assets);

  const dispatch = useDispatch();

  const handleValueAddress = useCallback((e) => {
    setFirstInput(false);
    setTokenAddress(e.target.value);
  }, []);

  const handleValueSymbol = useCallback((e) => {
    setFirstInput(false);
    setTokenSymbol(e.target.value);
  }, []);
  
  const addIsAdded = listAssets.find(
    (item) => item.tokenAddress === tokenAddress
  );
  const valid = web3.utils.isAddress(tokenAddress);

  const handleAddAsset = useCallback(() => {
    handleCancel();
    dispatch(addAssets({ tokenAddress, tokenSymbol }));
  }, [tokenAddress, tokenSymbol]);

  return (
    <div>
      <Modal
        footer={null}
        title="Import Token"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div className="wrapInputToken">
          <h5>Token Contract Address</h5>
          <input
            onChange={handleValueAddress}
            value={tokenAddress}
            type={"text"}
            className="tokenInput"
          />
          {!firtInput &&
            (valid ? (
              addIsAdded ? (
                <h5 className="not-valid">Address is already added</h5>
              ) : (
                <h5 className="valid">Address is valid!</h5>
              )
            ) : (
              <h5 className="not-valid">Address is not valid!</h5>
            ))}
          <h5>Token Symbol</h5>
          <input
            onChange={handleValueSymbol}
            value={tokenSymbol}
            type={"text"}
            className="tokenInput"
          />

          <button disabled={!valid || addIsAdded} onClick={handleAddAsset}>
            Add Custom Token
          </button>
        </div>
      </Modal>
    </div>
  );
};

export default ImportTokenModal;
