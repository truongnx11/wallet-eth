import React, { useMemo } from "react";
import { ReactComponent as EthSvg } from "assets/img/eth_logo.svg";
import "./Assets.scss";
import { useSelector } from "react-redux";
import ImportTokenModal from "./ImportTokenModal";
import useAssets from "talons/Home/Assets/useAssets";
import ItemAsset from "./ItemAsset";

const Assets = ({ balance,sendToken }) => {
  const listToken = useSelector((state) => state.assets.listToken);

  const {
    handleCancel,
    handleOk,
    isModalVisible,
    showModal,
    setIsModalVisible,
  } = useAssets();

  const content = useMemo(() => {
    if (!listToken) return <div />;
    else if (listToken.length === 0) return <div />;
    else
      return listToken.map((item,index) => (
        <ItemAsset sendToken={sendToken} count={index} key={item.symbol} item={item} />
      ));
  }, [listToken]);

  return (
    <>
      <ImportTokenModal
        handleCancel={handleCancel}
        handleOk={handleOk}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
      <ul className="root-asset">
        <li className="wrapItemAsset">
          <div className="wrap-eth-icon">
            <EthSvg width={30} />
          </div>
          <p>{balance.toString().slice(0, 6)} ETH</p>
        </li>
        {content}
        <li className="wrap-import">
          <p>Don't see your token?</p>
          <button onClick={showModal} className="btn-import">
            Import tokens
          </button>
        </li>
      </ul>
    </>
  );
};

export default React.memo(Assets);
