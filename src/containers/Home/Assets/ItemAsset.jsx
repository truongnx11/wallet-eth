import React, { useState } from "react";
import { ReactComponent as EthSvg } from "assets/img/eth_logo.svg";
import Jazzicon from "react-jazzicon";
import BigNumber from "bignumber.js";
import { Ether } from "utils/constants";
import { Modal, Button } from "antd";
import "./ItemAsset.scss";
import { useSelector } from "react-redux";
import Network from "constants/network";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const ItemAsset = ({ item, count, sendToken }) => {
  const [address, setAddress] = useState("");
  const [amount, setAmount] = useState(0);
  const [loading, setLoading] = useState(false);
  const networkSelected = useSelector((state) => state.network.key);

  const [linkAddress, setLinkAddress] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const { balance, symbol } = item;

  const handleSendToken = async () => {
    // 0x5592ec0cfb4dbc12d3ab100b257153436a1f0fea
    // handleCancel();
    setLoading(true);
    const { transactionHash } = await sendToken(item.address, address, amount);
    setLinkAddress(transactionHash);
    setLoading(false);
  };

  return (
    <li className="wrapItemToken">
      <Modal
        title="Basic Modal"
        visible={isModalVisible}
        footer={null}
        onCancel={handleCancel}
      >
        <h5>Address</h5>
        <input
          type="text"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
        />
        <h5>Amount</h5>
        <input
          type="number"
          value={amount}
          onChange={(e) => setAmount(e.target.value)}
        />
        {loading ? (
         <Spin indicator={antIcon} size="large" />
        ) : (
          <div className="btn-send-token">
            <button onClick={handleSendToken}>Send Token</button>
          </div>
        )}

        {linkAddress && (
          <a
            target={"_blank"}
            href={Network[networkSelected].tx_explorer + linkAddress}
          >
            See transaction
          </a>
        )}
      </Modal>

      <div onClick={showModal} className="wrapItemAsset">
        <div className="wrap-eth-icon">
          <Jazzicon diameter={30} seed={count + 100} />
        </div>
        <p>
          {balance} {symbol}
        </p>
      </div>
    </li>
  );
};

export default ItemAsset;
