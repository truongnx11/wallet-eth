const Network = {
  Offline: { rpc: "offline", tx_explorer: null },
  "Localhost 8545": { rpc: "http://127.0.0.1:8545", tx_explorer: null },
  "Ropsten Test Network": {
    rpc: "https://ropsten.infura.io/v3/3b4dd0b742974885b5f2142058b49bcf",
    tx_explorer: "https://ropsten.etherscan.io/tx/",
    api: "https://api-ropsten.etherscan.io",
    address: "https://ropsten.etherscan.io/address",
  },
  "Rinkeby Test Network": {
    rpc: "https://rinkeby.infura.io/v3/3b4dd0b742974885b5f2142058b49bcf",
    tx_explorer: "https://rinkeby.etherscan.io/tx/",
    api: "https://api-rinkeby.etherscan.io",
    address: "https://rinkeby.etherscan.io/address",
  },
  "Ethereum Mainnet": {
    rpc: "https://mainnet.infura.io/v3/3b4dd0b742974885b5f2142058b49bcf",
    tx_explorer: "https://etherscan.io/tx/",
    api: "https://api.etherscan.io",
    address: "https://etherscan.io/address",
  },
};

export default Network;
