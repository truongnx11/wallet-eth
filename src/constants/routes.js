const ROUTES = {
  HOME: "/",
  LOGIN: "/login",
  FIRST_SETUP: "/first-setup",
  CONGRATULATION: "/congratulation",
  RESTORE: '/restore',
};

export {ROUTES};
