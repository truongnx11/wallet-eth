import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Input, Space, Typography} from "antd";
import {EyeInvisibleOutlined, EyeTwoTone} from "@ant-design/icons";
import useCreatePassword from "hooks/useCreatePassword";
import actions from "redux/actions";
import {hashString} from "services/security";
import {createPassword} from "services/password";
import useLogin from "hooks/useLogin";

const {Title} = Typography;

const useRestoreWallet = () => {
  const dispatch = useDispatch();
  const [secretPhrase, setSecretPhrase] = useState();
  const [errorSecretPhrase, setErrorSecretPhrase] = useState(false);
  const {
    password,
    confirmPassword,
    errors,
    verifyPassword,
    handleChangePassword,
    handleChangeConfirmPassword,
  } = useCreatePassword();
  const {saveLoginState} = useLogin();

  const verifySecretPhrase = (phrase) => {
    if (!phrase) return false;
    const words = phrase.split(" ");
    if ([12, 15, 18, 21, 24].includes(words.length)) return true;
    return false;
  };

  const handleChangeSecretPhrase = (event) => {
    const {value: phrase} = event.target;
    setSecretPhrase(phrase);
    if (verifySecretPhrase(phrase)) {
      setErrorSecretPhrase(false);
      return;
    }
    setErrorSecretPhrase(true);
  };

  const handleRestoreWallet = () => {
    const satisfiedPw = verifyPassword();
    const satisfiedPhrase = verifySecretPhrase(secretPhrase);

    if (!satisfiedPhrase || !satisfiedPw) return false;

    createPassword(password);
    const passKey = hashString(password);
    dispatch(actions.keystore.createWallet(secretPhrase, passKey));
    saveLoginState(password);
    return true;
  };

  const renderForm = () => (
    <div>
      <div className="secret-phrase">
        <Input.Password
          size="large"
          placeholder="Enter your secret phrase"
          value={secretPhrase}
          onChange={handleChangeSecretPhrase}
          status={errorSecretPhrase ? "error" : ""}
        />
        {errorSecretPhrase && (
          <Typography className="error">
            Secret Recovery Phrases contain 12, 15, 18, 21, or 24 words
          </Typography>
        )}
      </div>

      <div className="password-creation">
        <Title level={4}>Create your password</Title>
        <Space direction="vertical">
          <Input.Password
            placeholder="Enter password"
            value={password}
            onChange={handleChangePassword}
            status={!!errors?.password ? "error" : ""}
          />
          <Typography className="error">{errors?.password}</Typography>
          <Input.Password
            placeholder="Confirm password"
            value={confirmPassword}
            onChange={handleChangeConfirmPassword}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            status={!!errors?.confirmPassword ? "error" : ""}
          />
          <Typography className="error">{errors?.confirmPassword}</Typography>
        </Space>
      </div>
    </div>
  );

  return {
    renderForm,
    handleRestoreWallet
  }
};

export default useRestoreWallet;
