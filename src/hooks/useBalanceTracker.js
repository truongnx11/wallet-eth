import Web3 from "web3";
import { erc20Abi } from "utils/abi";
const web3 = new Web3(); // eslint-disable-line
const erc20Contract = web3.eth.contract(erc20Abi);

const useBalanceTracker = () => {
  
    function getTokenBalancePromise(address, tokenContractAddress) {
    return new Promise((resolve, reject) => {
      const tokenContract = erc20Contract.at(tokenContractAddress);
      tokenContract.balanceOf.call(address, (err, balance) => {
        if (err) return reject(err);
        return resolve(balance);
      });
    });
  }

  return { getTokenBalancePromise };
};

export default useBalanceTracker;
