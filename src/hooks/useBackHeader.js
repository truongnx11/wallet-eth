const {PageHeader} = require("antd");
const {useHistory} = require("react-router-dom");

const useBackHeader = () => {
  const history = useHistory();

  const handleBack = () => {
    history.replace({search: ""});
  };

  const renderBackHeader = () => (
    <PageHeader
      className="site-page-header"
      onBack={handleBack}
      title="Metamask"
      subTitle="Back"
    />
  );

  return {renderBackHeader};
};

export default useBackHeader;
