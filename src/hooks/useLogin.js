import {useDispatch} from "react-redux";
import {setCookie} from "utils/cookie";
import {COOKIE_EXPIRES_TIME, COOKIE_LOGIN_LABEL} from "configs";
import actions from "redux/actions";
import {hashString} from "services/security";

const useLogin = () => {
  const dispatch = useDispatch();

  const saveLoginState = (password) => {
    const hashedPassword = hashString(password);

    setCookie(COOKIE_LOGIN_LABEL, hashedPassword, COOKIE_EXPIRES_TIME);
    dispatch(actions.auth.verifyLoggedInSuccess(hashedPassword));
    dispatch(actions.keystore.getKeystore());
  };

  return {saveLoginState};
};

export default useLogin;
