import {useState} from "react";
import { verifyCreatingPassword } from "services/password";

const useCreatePassword = () => {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errors, setErrors] = useState();

  const handleChangePassword = (event) => {
    setPassword(event.target.value);
    setErrors({...errors, password: null});
  };

  const handleChangeConfirmPassword = (event) => {
    setConfirmPassword(event.target.value);
    setErrors({...errors, confirmPassword: null});
  };

  const verifyPassword = () => {
    const errs = verifyCreatingPassword(password, confirmPassword);
    if (errs) {
      setErrors(errs);
      return false;
    }
    return true;
  }

  return {
    password,
    confirmPassword,
    errors,
    verifyPassword,
    handleChangePassword,
    handleChangeConfirmPassword,
  };
};

export default useCreatePassword;
