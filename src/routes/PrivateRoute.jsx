import React from "react";
import {useSelector} from "react-redux";
import {Route, Redirect} from "react-router-dom";
import {ROUTES} from "constants/routes";

const PrivateRoute = ({component: Component, ...rest}) => {
  const {isAuthenticated} = useSelector((state) => state.auth);
  const {failedKeystore} = useSelector((state) => state.keystore);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (failedKeystore) return <Redirect to={ROUTES.FIRST_SETUP} />;
        
        return isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={ROUTES.LOGIN} />
        );
      }}
    />
  );
};

export default PrivateRoute;
