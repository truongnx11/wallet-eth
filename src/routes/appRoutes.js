import {ROUTES} from "../constants/routes";
import Home from "../containers/Home";
import Login from "../containers/Login";
import FirstSetup from "../containers/FirstSetup";
import Congratulation from "containers/Congratulation";
import Restore from "containers/Restore";

export const appRoutes = [
  {
    path: ROUTES.LOGIN,
    component: Login,
    private: false,
  },
  {
    path: ROUTES.FIRST_SETUP,
    component: FirstSetup,
    private: false,
  },
  {
    path: ROUTES.CONGRATULATION,
    component: Congratulation,
    private: false,
  },
  {
    path: ROUTES.RESTORE,
    component: Restore,
    private: false,
  },
  {
    path: ROUTES.HOME,
    component: Home,
    private: true,
  },
];
