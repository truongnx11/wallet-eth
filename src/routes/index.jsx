import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {BrowserRouter, Redirect, Switch} from "react-router-dom";
import {appRoutes} from "./appRoutes";
import PrivateRoute from "./PrivateRoute";
import {ROUTES} from "../constants/routes";
import PublicRoute from "./PublicRoute";
import actions from "redux/actions";

const PrivateApp = () => {
  const privateRoutes = appRoutes.filter((route) => route.private);

  return (
    // TODO: put Layout here
    <div>
      <Switch>
        {privateRoutes.map((privateRoute) => (
          <PrivateRoute
            path={privateRoute.path}
            component={privateRoute.component}
            exact
            key={privateRoute.path}
          />
        ))}
        <Redirect to={ROUTES.HOME} />
      </Switch>
    </div>
  );
};

const AppRouter = () => {
  const dispatch = useDispatch();
  const {verifying} = useSelector((state) => state.auth);
  const [isFirstTime, setIsFirstTime] = useState(true);

  const publicRoutes = appRoutes.filter((route) => !route.private);

  useEffect(() => {
    dispatch(actions.auth.verifyLoggedIn());
    dispatch(actions.keystore.getKeystore());
    setIsFirstTime(false);
  }, [dispatch]);

  if (verifying || isFirstTime) {
    return <div>Loading....</div>;
  }

  return (
    <BrowserRouter>
      <Switch>
        {publicRoutes.map((route) => (
          <PublicRoute
            key={route.path}
            path={route.path}
            exact
            component={route.component}
          />
        ))}
        <PrivateApp />
      </Switch>
    </BrowserRouter>
  );
};

export default AppRouter;
