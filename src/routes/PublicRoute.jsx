import React from "react";
import {Redirect, Route, useLocation} from "react-router-dom";
import {useSelector} from "react-redux";
import {ROUTES} from "constants/routes";

const PublicRoute = ({component: Component, ...rest}) => {
  const location = useLocation();
  const {isAuthenticated} = useSelector((state) => state.auth);
  const {failedKeystore} = useSelector((state) => state.keystore);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (failedKeystore) {
          if (!location.pathname.includes(ROUTES.FIRST_SETUP))
            return <Redirect to={ROUTES.FIRST_SETUP} />;
          return <Component {...props} />;
        }

        if (isAuthenticated) {
          return <Redirect to={ROUTES.HOME} />;
        } else {
          return [ROUTES.LOGIN, ROUTES.RESTORE].includes(location.pathname) ? (
            <Component {...props} />
          ) : (
            <Redirect to={ROUTES.LOGIN} />
          );
        }
      }}
    />
  );
};

export default PublicRoute;
