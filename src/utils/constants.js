export const RESTART_ON_REMOUNT = "@@saga-injector/restart-on-remount";
export const DAEMON = "@@saga-injector/daemon";
export const ONCE_TILL_UNMOUNT = "@@saga-injector/once-till-unmount";

/* Auto generated password lenght */
export const generatedPasswordLength = 12;

/* Default HD path string for key generation from seed */
export const hdPathString = `m/44'/60'/0'/0`; // eslint-disable-line

// time in ms for check balancess polling
export const timeBetweenCheckbalances = 180 * 1000;

/* Max gas for send transaction (not gas price) */
export const maxGasForEthSend = 25000;
/* Max gas for token send transaction (not gas price) */
export const maxGasForTokenSend = 60000;

/* Eth unit constants will be saved as strings to prevent accidental manipulation
    usage: convert amount to wei
    const sendAmount = new BigNumber(amount).times(Ether);
*/
export const Ether = (1.0e18).toString();
export const Gwei = (1.0e9).toString();

/* offline mode is special case of error */
export const offlineModeString = "Offline";
/* Default network to connect after wallet creation (see network.js) */
export const defaultNetwork = "Ropsten Testnet";

/* keystore will be saved to local storage under this key */
export const localStorageKey = "ks";

// addresses:
export const website = "https://eth-hot-wallet.com";
export const github = "https://github.com/PaulLaux/eth-hot-wallet";

// APIs:
// export const checkFaucetAddress = 'http://localhost:3000/status';
// export const askFaucetAddress = 'http://localhost:3000/ask';
export const checkFaucetAddress =
  "https://m6b19m0fxh.execute-api.eu-west-1.amazonaws.com/dev/status";
export const askFaucetAddress =
  "https://m6b19m0fxh.execute-api.eu-west-1.amazonaws.com/dev/ask";

const listMonth = [
  "Jan", //0
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export const convertNumToMonth = (month) => {
  return listMonth[month];
};

export const IconSend = (
  <svg
    width="28"
    height="28"
    viewBox="0 0 30 30"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="0.5"
      y="0.5"
      width="29"
      height="29"
      rx="14.5"
      stroke="#2F80ED"
    ></rect>
    <path
      d="M18.5851 9.88921C18.5586 9.89005 18.5321 9.89238 18.5057 9.89618H14.3207C14.0635 9.89254 13.8243 10.0276 13.6947 10.2497C13.565 10.4719 13.565 10.7466 13.6947 10.9687C13.8243 11.1908 14.0635 11.3259 14.3207 11.3222H16.8777L9.53811 18.6614C9.35182 18.8402 9.27679 19.1058 9.34193 19.3557C9.40707 19.6056 9.60222 19.8007 9.85211 19.8658C10.102 19.931 10.3676 19.8559 10.5464 19.6697L17.886 12.3305V14.8874C17.8823 15.1445 18.0175 15.3837 18.2396 15.5133C18.4617 15.643 18.7364 15.643 18.9585 15.5133C19.1806 15.3837 19.3158 15.1445 19.3121 14.8874V10.6997C19.3409 10.4919 19.2767 10.282 19.1366 10.1259C18.9965 9.96973 18.7948 9.88316 18.5851 9.88921Z"
      fill="#2F80ED"
    ></path>
  </svg>
);

export const iconRecieve = (
  <svg
    width="28"
    height="28"
    viewBox="0 0 28 28"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="0.5"
      y="0.5"
      width="27"
      height="27"
      rx="13.5"
      stroke="#2F80ED"
    ></rect>
    <path
      d="M14.3465 17.3611C14.3661 17.3402 14.3846 17.3181 14.4018 17.2952L17.597 14.0999C17.7961 13.9063 17.8756 13.6206 17.805 13.352C17.7344 13.0834 17.5246 12.8737 17.2561 12.8031C16.9875 12.7325 16.7017 12.812 16.5082 13.0111L14.5559 14.9633L14.5559 7.25598C14.5616 6.97721 14.4161 6.71715 14.1756 6.5761C13.9351 6.43505 13.6371 6.43505 13.3966 6.5761C13.1561 6.71715 13.0106 6.97721 13.0163 7.25598L13.0163 14.9633L11.064 13.0111C10.8705 12.812 10.5847 12.7325 10.3161 12.8031C10.0476 12.8737 9.83782 13.0834 9.76721 13.352C9.69661 13.6206 9.77608 13.9063 9.97519 14.0999L13.1726 17.2973C13.3093 17.4779 13.5186 17.5891 13.7447 17.6014C13.9709 17.6137 14.191 17.5258 14.3465 17.3611Z"
      fill="#2F80ED"
    ></path>
    <rect
      x="7.875"
      y="19.25"
      width="12.25"
      height="1.75"
      rx="0.875"
      fill="#2F80ED"
    ></rect>
  </svg>
);

export const iconUpdate = (
  <svg
    width="28"
    height="28"
    viewBox="0 0 30 30"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M15 29C22.732 29 29 22.732 29 15C29 7.26801 22.732 1 15 1C7.26801 1 1 7.26801 1 15C1 22.732 7.26801 29 15 29Z"
      stroke="#2F80ED"
    ></path>
    <path
      d="M18.8889 18.65C18.8889 18.8433 18.7322 19 18.5389 19H11.4611C11.2678 19 11.1111 18.8433 11.1111 18.65V17.4621C11.1111 17.1479 10.7292 16.9928 10.5102 17.2181L8.2372 19.556C8.10513 19.6919 8.10513 19.9081 8.2372 20.044L10.5102 22.3819C10.7292 22.6072 11.1111 22.4521 11.1111 22.1379V20.95C11.1111 20.7567 11.2678 20.6 11.4611 20.6H20.0944C20.2877 20.6 20.4444 20.4433 20.4444 20.25V16.15C20.4444 15.9567 20.2877 15.8 20.0944 15.8H19.2389C19.0456 15.8 18.8889 15.9567 18.8889 16.15V18.65ZM11.1111 12.35C11.1111 12.1567 11.2678 12 11.4611 12H18.5389C18.7322 12 18.8889 12.1567 18.8889 12.35V13.5379C18.8889 13.8521 19.2708 14.0072 19.4898 13.7819L21.7628 11.444C21.8949 11.3081 21.8949 11.0919 21.7628 10.956L19.4898 8.61812C19.2708 8.39284 18.8889 8.5479 18.8889 8.8621V10.05C18.8889 10.2433 18.7322 10.4 18.5389 10.4H9.90556C9.71226 10.4 9.55556 10.5567 9.55556 10.75V14.85C9.55556 15.0433 9.71226 15.2 9.90556 15.2H10.7611C10.9544 15.2 11.1111 15.0433 11.1111 14.85V12.35Z"
      fill="#2F80ED"
    ></path>
  </svg>
);

export const subText = (text) =>
  `${text.slice(0, 5)}...${text.slice(text.length - 4, text.length)}`;

export const DEFAULT_ITEM = 5;