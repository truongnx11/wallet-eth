const save = (key, data) => {
  if (typeof data === "string") localStorage.setItem(key, data);
  else localStorage.setItem(key, JSON.stringify(data));
};

const find = (key) => {
  return localStorage.getItem(key);
};

export {save, find};
