export const actionTypes = {
  GET_KEYSTORE: "GET_KEYSTORE",
  GET_KEYSTORE_SUCCESS: "GET_KEYSTORE_SUCCESS",
  GET_KEYSTORE_FAILURE: "GET_KEYSTORE_FAILURE",
  CREATE_WALLET: "CREATE_WALLET",
  CREATE_ACCOUNT: "CREATE_ACCOUNT",
  CREATE_ACCOUNT_SUCCESS: "CREATE_ACCOUNT_SUCCESS",
};

export const getKeystore = () => ({
  type: actionTypes.GET_KEYSTORE,
});

export const getKeystoreSuccess = (mnemonic, accounts) => ({
  type: actionTypes.GET_KEYSTORE_SUCCESS,
  payload: {mnemonic, accounts},
});

export const getKeystoreFailure = () => ({
  type: actionTypes.GET_KEYSTORE_FAILURE,
});

export const createWallet = (mnemonic, passKey) => ({
  type: actionTypes.CREATE_WALLET,
  payload: {mnemonic, passKey},
});

export const createAccount = (accountName, passKey) => ({
  type: actionTypes.CREATE_ACCOUNT,
  payload: {accountName, passKey},
});

export const createAccountSuccess = (account) => ({
  type: actionTypes.CREATE_ACCOUNT_SUCCESS,
  payload: {account},
});
