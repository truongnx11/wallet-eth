import {actionTypes} from "./actions";

export const initialState = {
  mnemonic: "",
  accounts: [], // { address, name, privateKey, publicKey }
  failedKeystore: false,
};

export default function keystoreReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_KEYSTORE: {
      return state;
    }

    case actionTypes.GET_KEYSTORE_SUCCESS: {
      const {mnemonic, accounts} = action.payload || {};
      return {...state, mnemonic, accounts, failedKeystore: false};
    }

    case actionTypes.GET_KEYSTORE_FAILURE:
      return {...state, failedKeystore: true};

    case actionTypes.CREATE_WALLET: {
      return state;
    }

    case actionTypes.CREATE_ACCOUNT: {
      return state;
    }

    case actionTypes.CREATE_ACCOUNT_SUCCESS: {
      const {account} = action.payload;
      return {...state, accounts: [...state.accounts, account]};
    }

    default:
      return state;
  }
}
