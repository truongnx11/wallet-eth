import {all, put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
  getAccounts,
  saveAccounts,
  saveMnemonic,
  getMnemonic,
  isKeystoreEmpty,
  saveAccount,
  getTotalAccounts,
} from "services/keystore";
import actions from "../actions";
import {ERROR_CODE} from "errors/code";
import {getCookie} from "utils/cookie";
import {COOKIE_LOGIN_LABEL} from "configs";
import {
  createAccount as createNewAccount,
  getActiveAccountIndex,
  setActiveAccountIndex,
} from "services/account";

function* getKeystore() {
  try {
    if (isKeystoreEmpty()) {
      throw new Error("keystore is empty");
    }

    const passKey = getCookie(COOKIE_LOGIN_LABEL);

    if (!passKey) {
      // passKey null => log in failed
      yield put(actions.auth.verifyLoggedInFailure());
      return;
    }

    const mnemonic = getMnemonic(passKey);
    const accounts = getAccounts(passKey);
    if (!mnemonic || !accounts.length) throw new Error("Get keystore error");

    // TODO: handle network, check password, save seed from local storage

    const accountIndex = getActiveAccountIndex() || 0;

    yield put(actions.keystore.getKeystoreSuccess(mnemonic, accounts));
    yield put(actions.account.setAccount(accounts[accountIndex], accountIndex));
  } catch (error) {
    if (error.code === ERROR_CODE.WRONG_PASS_KEY)
      yield put(actions.auth.verifyLoggedInFailure());
    else yield put(actions.keystore.getKeystoreFailure());
  }
}

function* createWallet({payload}) {
  try {
    const {mnemonic, passKey} = payload;
    if (!passKey) {
      throw new Error("passKey is null");
    }

    const wallet = createNewAccount(mnemonic, 0);
    const {privateKey, address, publicKey} = wallet;
    const firstAccount = {name: "Account 1", address, privateKey, publicKey};

    saveMnemonic(mnemonic, passKey);
    saveAccounts([firstAccount], passKey);
    yield put(actions.keystore.getKeystoreSuccess(mnemonic, [firstAccount]));
    yield put(actions.account.setAccount(firstAccount, 0));
    toast.success("Create Wallet Successfully");
  } catch (error) {
    toast.error("Create Wallet error: " + error.message);
  }
}

function* createAccountSaga({payload}) {
  try {
    const {accountName, passKey} = payload;

    const totalAccount = getTotalAccounts(passKey);
    const mnemonic = getMnemonic(passKey);
    const newAccount = createNewAccount(mnemonic, totalAccount);
    const {address, privateKey, publicKey} = newAccount;

    const nextAccount = {
      name: accountName,
      address,
      privateKey,
      publicKey,
    };
    const index = saveAccount(nextAccount, passKey);
    setActiveAccountIndex(index);
    yield put(actions.account.setAccount(nextAccount, index));
    yield put(actions.keystore.createAccountSuccess(nextAccount));

    toast.success("Create new account success");
  } catch (error) {
    toast.error("Create Account error: " + error.message);
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.keystore.actionTypes.GET_KEYSTORE, getKeystore),
    takeEvery(actions.keystore.actionTypes.CREATE_WALLET, createWallet),
    takeEvery(actions.keystore.actionTypes.CREATE_ACCOUNT, createAccountSaga),
  ]);
}
