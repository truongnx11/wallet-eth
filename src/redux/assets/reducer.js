import { STORAGE_KEYS } from "constants";
import { find } from "utils/storage";
import { actionTypes } from "./actions";

export const initialState = {
  assets: JSON.parse(find(STORAGE_KEYS.ASSETS)) || [],
  listToken: null,
};

export default function assetsReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_LIST_ASSETS_SUCCESS: {
      const listToken = action.payload;
      return { ...state, listToken };
    }

    case actionTypes.ADD_ASSET_SUCCESS: {
      // asset = {address,symbol}
      const listAssets = action.payload;
      return { ...state, assets: listAssets };
    }

    default:
      return state;
  }
}
