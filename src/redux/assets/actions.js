export const actionTypes = {
  GET_LIST_ASSETS: "GET_LIST_ASSETS",
  GET_LIST_ASSETS_SUCCESS: "GET_LIST_ASSETS_SUCCESS",
  ADD_ASSET:'ADD_ASSET',
  ADD_ASSET_SUCCESS:'ADD_ASSET_SUCCESS'
};

export const getListAssets = (payload) => ({
  type: actionTypes.GET_LIST_ASSETS,
  payload
});

export const getListAssetsSuccess = (payload) => ({
  type: actionTypes.GET_LIST_ASSETS_SUCCESS,
  payload,
});

export const addAssets = (payload) => ({
  type: actionTypes.ADD_ASSET,
  payload,
});

export const addAssetsSuccess = (payload) => ({
  type: actionTypes.ADD_ASSET_SUCCESS,
  payload,
});