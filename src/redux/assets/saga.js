import { all, put, takeEvery } from "redux-saga/effects";
import actions from "../actions";
import { find, save } from "utils/storage";
import { STORAGE_KEYS } from "constants";
import Web3 from "web3";
import { erc20Abi } from "utils/abi";
import { getListAssetsSuccess } from "./actions";
import Network from "constants/network";
import { DEFAULT_NETWORK_KEY } from "constants";
import { toast } from "react-toastify";

const getTokenBalanceContract = async (
  address,
  tokenContractAddress,
  networkParam
) => {
  const web3 = new Web3(
    networkParam ? Network[networkParam].rpc : Network[DEFAULT_NETWORK_KEY].rpc
  );
  const tokenContract = new web3.eth.Contract(erc20Abi, tokenContractAddress);
  const balance = await tokenContract.methods.balanceOf(address).call();
  const symbol = await tokenContract.methods.symbol().call();
  const decimals = await tokenContract.methods.decimals().call();
  return {
    balance: +balance / 10 ** +decimals,
    symbol,
    tokenContractAddress,
    decimals,
  };
};

function* getListAssets({
  payload: { addressAccount, networkSelected: networkParam },
}) {
  const listAssets = JSON.parse(find(STORAGE_KEYS.ASSETS)) || [];
  const newData = [];

  const handleGetListTokenBalance = async (listData) => {
    try {
      for (const item of listData) {
        const { balance, symbol, tokenContractAddress, decimals } =
          await getTokenBalanceContract(
            addressAccount,
            item.tokenAddress,
            networkParam
          );
        newData.push({
          symbol: symbol ? symbol : item.tokenSymbol,
          balance,
          address: tokenContractAddress,
          decimals,
        });
      }
    } catch (error) {
      toast.error("Can't get list token!");
      console.log({ error });
    }
  };

  if (listAssets.length > 0) {
    yield handleGetListTokenBalance(listAssets);
    yield put(getListAssetsSuccess(newData));
  }
}

function* handleAddAsset({ payload }) {
  const listAssets = JSON.parse(find(STORAGE_KEYS.ASSETS)) || [];
  listAssets.push(payload);
  save(STORAGE_KEYS.ASSETS, JSON.stringify(listAssets));
  yield put(actions.assets.addAssetsSuccess(listAssets));
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.assets.actionTypes.GET_LIST_ASSETS, getListAssets),
    takeEvery(actions.assets.actionTypes.ADD_ASSET, handleAddAsset),
  ]);
}
