import { STORAGE_KEYS, DEFAULT_NETWORK_KEY } from "constants";
import { find, save } from "utils/storage";
import { actionTypes } from "./actions";

export const initialState = {
  key: find(STORAGE_KEYS.NETWORK) || DEFAULT_NETWORK_KEY,
};

export default function networkReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.CHANGE_NETWORK: {
      const { key } = action.payload;
      save(STORAGE_KEYS.NETWORK, key);
      return { key };
    }

    default:
      return state;
  }
}
