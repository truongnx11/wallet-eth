export const actionTypes = {
  CHANGE_NETWORK: "CHANGE_NETWORK",
};

export const changeNetwork = (key) => ({
  type: actionTypes.CHANGE_NETWORK,
  payload: { key },
});
