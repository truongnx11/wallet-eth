import * as account from "./account/actions";
import * as keystore from "./keystore/actions";
import * as auth from './auth/actions';
import * as assets from './assets/actions';

const actions = {
  account,
  keystore,
  auth,
  assets
};

export default actions;
