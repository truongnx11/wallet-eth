export const actionTypes = {
  VERIFY_LOGGED_IN: "VERIFY_LOGGED_IN",
  VERIFY_LOGGED_IN_SUCCESS: "VERIFY_LOGGED_IN_SUCCESS",
  VERIFY_LOGGED_IN_FAILURE: "VERIFY_LOGGED_IN_FAILURE",
  SET_PASS_KEY_ON_SETUP: "SET_PASS_KEY_ON_SETUP",
  USER_LOGOUT:'USER_LOGOUT',
  USER_LOGOUT_SUCCESS:'USER_LOGOUT_SUCCESS'
};

const verifyLoggedIn = () => ({
  type: actionTypes.VERIFY_LOGGED_IN,
});

const verifyLoggedInSuccess = (passKey) => ({
  type: actionTypes.VERIFY_LOGGED_IN_SUCCESS,
  payload: {passKey},
});

const verifyLoggedInFailure = () => ({
  type: actionTypes.VERIFY_LOGGED_IN_FAILURE,
});

const setPassKeyOnFirstSetup = (passKey) => ({
  type: actionTypes.SET_PASS_KEY_ON_SETUP,
  payload: {passKey},
});

const handleLogoutUser = () => ({
  type: actionTypes.USER_LOGOUT,
});

const handleLogoutUserSuccess = () => ({
  type: actionTypes.USER_LOGOUT_SUCCESS,
});


export {
  verifyLoggedIn,
  verifyLoggedInSuccess,
  verifyLoggedInFailure,
  setPassKeyOnFirstSetup,
  handleLogoutUser,
  handleLogoutUserSuccess
};
