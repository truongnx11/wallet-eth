import { actionTypes } from "./actions";

export const initialState = {
  passKey: null,
  isAuthenticated: false,
  verifying: false,
  passKeyOnSetup: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.VERIFY_LOGGED_IN:
      return { ...state, verifying: true };
    case actionTypes.VERIFY_LOGGED_IN_SUCCESS: {
      const { passKey } = action.payload;
      return { ...state, verifying: false, passKey, isAuthenticated: true };
    }
    case actionTypes.VERIFY_LOGGED_IN_FAILURE:
      return { ...state, verifying: false, isAuthenticated: false };

    case actionTypes.SET_PASS_KEY_ON_SETUP: {
      const { passKey } = action.payload;
      return { ...state, passKeyOnSetup: passKey };
    }

    case actionTypes.USER_LOGOUT_SUCCESS: {
      return { ...state, passKey: null, isAuthenticated: false };
    }

    default:
      return state;
  }
};

export default authReducer;
