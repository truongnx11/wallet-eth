import { COOKIE_LOGIN_LABEL } from 'configs';
import { all, put, takeEvery } from 'redux-saga/effects';
import { eraseCookie, getCookie } from 'utils/cookie';
import actions from '../actions';
import { handleLogoutUserSuccess } from './actions';

function* verifyLoggedInSaga() {
  try {

    const passKey = getCookie(COOKIE_LOGIN_LABEL);

    if (!passKey) {
      yield put(actions.auth.verifyLoggedInFailure());
      return;
    }

    yield put(actions.auth.verifyLoggedInSuccess(passKey));
  } catch (error) {
    yield put(actions.auth.verifyTokenFailure());
  }
}

function* handleUserLogout() {
   eraseCookie(COOKIE_LOGIN_LABEL);
  yield put(handleLogoutUserSuccess());
  
  // yield eraseCookie(COOKIE_LOGIN_LABEL);
}


export default function* rootSaga() {
  yield all([
    takeEvery(actions.auth.actionTypes.VERIFY_LOGGED_IN, verifyLoggedInSaga),
    takeEvery(actions.auth.actionTypes.USER_LOGOUT, handleUserLogout),
  ]);
}
