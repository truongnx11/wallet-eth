import { combineReducers } from "redux";
import account, {
  initialState as accountInitialState,
} from "./account/reducer";
import keystore, {
  initialState as keystoreInitialState,
} from "./keystore/reducer";
import network, {
  initialState as networkInitialState,
} from "./network/reducer";
import transactions, {
  initialState as transactionsInitialState,
} from "./transactions/reducer";
import auth, { initialState as authInitialState } from "./auth/reducer";
import assets, { initialState as assetsInitialState } from "./assets/reducer";

export const initialState = {
  auth: authInitialState,
  account: accountInitialState,
  keystore: keystoreInitialState,
  network: networkInitialState,
  transactions: transactionsInitialState,
  assets: assetsInitialState,
};

export default combineReducers({
  auth,
  account,
  keystore,
  network,
  transactions,
  assets,
});
