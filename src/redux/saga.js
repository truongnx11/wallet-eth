import { all } from "redux-saga/effects";
import keystore from "./keystore/saga";
import transaction from "./transactions/saga";
import auth from "./auth/saga";
import assets from "./assets/saga";

function* rootSaga() {
  yield all([keystore(), transaction(), auth(), assets()]);
}

export default rootSaga;
