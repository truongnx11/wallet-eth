export const actionTypes = {
  SET_ACCOUNT: "SET_ACCOUNT",
};

export const setAccount = (account, accountIndex) => ({
  type: actionTypes.SET_ACCOUNT,
  payload: {account, accountIndex},
});
