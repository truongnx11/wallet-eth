import {actionTypes} from "./actions";

export const initialState = {
  address: "",
  name: "",
  privateKey: "",
  publicKey: "",
  accountIndex: 0,
};

export default function accountReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_ACCOUNT: {
      const {account = {}, accountIndex} = action.payload;
      return {...state, ...account, accountIndex};
    }

    default:
      return state;
  }
}
