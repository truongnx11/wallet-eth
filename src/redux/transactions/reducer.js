import { actionTypes } from "./actions";

export const initialState = {
  listTransaction: null,
  page: 1,
  isLoading: false,
  canLoadMore: true,
};

export default function transactionReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_TRANSACTIONS_SUCCESS: {
      const listData = action.payload;
      return { ...state, listTransaction: listData, isLoading: false };
    }
    case actionTypes.LOADMORE_TRANSACTION_SUCCESS: {
      const listData = action.payload;
      return {
        ...state,
        listTransaction: [...state.listTransaction, ...listData],
        page: state.page + 1,
        isLoading: false,
      };
    }
    case actionTypes.CAN_NOT_LOAD_MORE: {
      return {
        ...state,
        canLoadMore: false,
        isLoading: false,
      };
    }
    case actionTypes.LOADING_TRANSACTION: {
      return {
        ...state,
        isLoading: true,
      };
    }

    default:
      return state;
  }
}
