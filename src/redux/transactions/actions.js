export const actionTypes = {
  GET_TRANSACTIONS: "GET_TRANSACTIONS",
  GET_TRANSACTIONS_SUCCESS: "GET_TRANSACTIONS_SUCCESS",
  LOADMORE_TRANSACTION: "LOADMORE_TRANSACTION",
  LOADMORE_TRANSACTION_SUCCESS: "LOADMORE_TRANSACTION_SUCCESS",
  CAN_NOT_LOAD_MORE: "CAN_NOT_LOAD_MORE",
  LOADING_TRANSACTION: "LOADING_TRANSACTION",
};

export const getTransactions = (payload) => ({
  type: actionTypes.GET_TRANSACTIONS,
  payload,
});

export const getTransactionsSuccess = (payload) => ({
  type: actionTypes.GET_TRANSACTIONS_SUCCESS,
  payload,
});

export const loadMoreTransactions = (payload) => ({
  type: actionTypes.LOADMORE_TRANSACTION,
  payload,
});

export const loadMoreTransactionsSuccess = (payload) => ({
  type: actionTypes.LOADMORE_TRANSACTION_SUCCESS,
  payload,
});

export const canNotLoadMore = () => ({
  type: actionTypes.CAN_NOT_LOAD_MORE,
});

export const showLoadingTransaction = () => ({
  type: actionTypes.LOADING_TRANSACTION
});