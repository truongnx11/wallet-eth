import { all, call, put, takeEvery } from "redux-saga/effects";
import TransactionService from "services/network.services";
import { DEFAULT_ITEM } from "utils/constants";
import {
  actionTypes,
  canNotLoadMore,
  getTransactionsSuccess,
  loadMoreTransactionsSuccess,
  showLoadingTransaction,
} from "./actions";

const defaultParam = {
  module: "account",
  action: "txlist",
  startblock: 0,
  endblock: 99999999,
  page: 1,
  offset: DEFAULT_ITEM,
  sort: "asc",
  apikey: "PVYR8JTDNXDXD7VS96XXR76ZMI5HRS688Z",
};
// fee = gas_price x used_gas
// address
// network

function* getListTransaction({ payload }) {
  yield put(showLoadingTransaction());
  const network = payload.network;
  delete payload.network;
  const params = { ...payload, ...defaultParam };
  try {
    const res = yield call(TransactionService.listTransaction, {
      baseUrl: network,
      enpoint: { ...params },
    });

    yield put(getTransactionsSuccess(res.data.result));
    if (res.data.result < DEFAULT_ITEM) {
      yield put(canNotLoadMore());
    }
  } catch (error) {
    console.log(error);
  }
}
function* loadMoreTransaction({ payload }) {
  yield put(showLoadingTransaction());
  const { network, page } = payload;
  delete payload.network;
  const params = { ...payload, ...defaultParam, page };

  try {
    const res = yield call(TransactionService.listTransaction, {
      baseUrl: network,
      enpoint: { ...params },
    });
    const result = res.data.result;
    if (result.length === DEFAULT_ITEM) {
      yield put(loadMoreTransactionsSuccess(result));
    } else if (result.length > 0) {
      yield put(loadMoreTransactionsSuccess(result));
      yield put(canNotLoadMore());
    } else {
      yield put(canNotLoadMore());
    }
  } catch (error) {
    console.log(error);
  }
}

export default function* rootSaga() {
  yield all([takeEvery(actionTypes.GET_TRANSACTIONS, getListTransaction)]);
  yield all([takeEvery(actionTypes.LOADMORE_TRANSACTION, loadMoreTransaction)]);
}
