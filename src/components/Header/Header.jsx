import React, {useState} from "react";
import Jazzicon from "react-jazzicon";
import {Select} from "antd";
import useHeader from "talons/Header/useHeader";
import {useSelector} from "react-redux";
import Logo from "assets/img/pixlr-bg-result.png";
import AccountPanel from "components/AccountPanel";
import "./Header.scss";

const {Option} = Select;

const Header = () => {
  const {handleChangeNetwork, handleLogout} = useHeader();
  const networkSelected = useSelector((state) => state.network.key);
  const [openPopover, setOpenPopover] = useState(false);
  const { accountIndex = 0 } = useSelector((state) => state.account);

  const handleOpenPopover = () => {
    setOpenPopover(true);
  };

  const handleVisibleChange = (visible) => {
    setOpenPopover(visible);
  };

  return (
    <div className="main-page-header">
      <div className="left-header">
        <img src={Logo} className="logo" alt="" />
        <h4>SMART-MASK</h4>
      </div>
      <div className="right-header">
        <button onClick={handleLogout} className="btn-lock">
          Lock
        </button>
        <Select
          showSearch
          defaultValue={networkSelected}
          style={{width: 200, background: "transparent"}}
          placeholder="Search to Select"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
          filterSort={(optionA, optionB) =>
            optionA.children
              .toLowerCase()
              .localeCompare(optionB.children.toLowerCase())
          }
          onChange={handleChangeNetwork}
        >
          <Option value="Ethereum Mainnet">Ethereum Mainnet</Option>
          <Option value="Ropsten Test Network">Ropsten Test Network</Option>
          <Option value="Rinkeby Test Network">Rinkeby Test Network</Option>
          <Option value="Localhost 8545">Localhost 8545</Option>
        </Select>
        <div className="icon-rect" onClick={handleOpenPopover}>
          <Jazzicon diameter={35} seed={accountIndex} />
        </div>
        <AccountPanel
          open={openPopover}
          onVisibleChange={handleVisibleChange}
        />
      </div>
    </div>
  );
};

export default React.memo(Header);
