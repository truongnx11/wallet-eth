import Network from "constants/network";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { subText } from "utils/constants";
import "./TopAccount.scss";

const TopAccount = () => {
  const [showOption, setShowOption] = useState(false);
  const networkSelected = useSelector((state) => state.network.key);
  const handleShowOption = () => {
    setShowOption(!showOption);
  };
  const account = useSelector((state) => state.account);

  const copyText = () => {
    // loading the content into our clipboard
    navigator.clipboard.writeText(account.address);
  };

  return (
    <div className="top-account">
      <div className="account-name">
        <h3 className="name-user">{account.name}</h3>
        <p className="address-account" onClick={copyText}>
          {subText(account.address)}{" "}
          <span>
            <svg
              width="11"
              height="11"
              viewBox="0 0 11 11"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M0 0H1H9V1H1V9H0V0ZM2 2H11V11H2V2ZM3 3H10V10H3V3Z"
                fill="#6a737d"
              ></path>
            </svg>
          </span>
        </p>
      </div>
      <div className="three-dot">
        <div onClick={handleShowOption}>
          <div className="dot"></div>
          <div className="dot"></div>
          <div className="dot"></div>
        </div>
        {showOption && (
          <ul className="option-account">
            <li>
                <a target={'_blank'} href={`${Network[networkSelected].address}/${account.address}`}>View Account on Etherscan</a>
            </li>
            <li>Account details</li>
            <li>Connected sites</li>
          </ul>
        )}
      </div>
    </div>
  );
};

export default React.memo(TopAccount);
