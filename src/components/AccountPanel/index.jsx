import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Input, List, Popover, Space, Typography} from "antd";
import {CheckCircleTwoTone} from "@ant-design/icons";
import {toast} from "react-toastify";
import Jazzicon from "react-jazzicon";
import "./index.style.scss";
import actions from "redux/actions";
import {setActiveAccountIndex} from "services/account";


const {Title} = Typography;

const initialNewAccount = {
  creating: false,
  name: "",
  placeholderName: "",
  error: null,
};

const AccountPanel = ({open, onVisibleChange}) => {
  const dispatch = useDispatch();
  const {accounts} = useSelector((state) => state.keystore);
  const {address} = useSelector((state) => state.account);
  const {passKey} = useSelector((state) => state.auth);

  const [newAccount, setNewAccount] = useState(initialNewAccount);

  const handleSelectAccount = (account) => {
    if (account.address === address) return;

    // Save active account index
    const index = accounts.findIndex(
      (element) => element.address === account.address
    );
    dispatch(actions.account.setAccount(account, index));
    setActiveAccountIndex(index);
    onVisibleChange(false);
  };

  const isActive = (item) => item.address === address;

  const handleClickNewAccount = () => {
    const accountName = `Account ${accounts.length}`;
    setNewAccount({creating: true, placeholderName: accountName});
  };

  const handleVisibleChange = (visible) => {
    setNewAccount(initialNewAccount);
    onVisibleChange(visible);
  };

  const handleChangeInputNewAccount = (event) =>
    setNewAccount((prev) => ({
      ...prev,
      error: null,
      name: event.target.value,
    }));

  const handleCreateNewAccount = () => {
    let accountName = newAccount.placeholderName;
    if (newAccount.name) accountName = newAccount.name;
    const existedAccount = accounts.find((item) => item.name === accountName);
    if (existedAccount) {
      setNewAccount({
        ...newAccount,
        error: "This name already existed",
      });
      return;
    }

    if (passKey) {
      dispatch(actions.keystore.createAccount(accountName, passKey));
      handleVisibleChange(false);
    }
    else toast.error("PassKey is null, please reload page");
  };

  const Content = () => (
    <div className="content-wrapper">
      <div className="divider" />
      <List
        dataSource={accounts}
        renderItem={(item, index) => (
          <List.Item
            key={item.address}
            className={`account-item ${isActive(item) && "active"}`}
            onClick={() => handleSelectAccount(item)}
          >
            {isActive(item) ? (
              <CheckCircleTwoTone />
            ) : (
              <div className="space" />
            )}
            <Jazzicon diameter={35} seed={index} />
            <div>
              <Title level={5} className="name">
                {item.name}
              </Title>
              <Typography className="balance">1.2 ETH</Typography>
            </div>
          </List.Item>
        )}
      />
      <div className="divider" />
      {newAccount.creating && (
        <div className="new-account">
          <Space direction="horizontal" align="center">
            <Input
              placeholder={newAccount.placeholderName}
              value={newAccount.name}
              onChange={handleChangeInputNewAccount}
            />
            <Button onClick={handleCreateNewAccount}>Create</Button>
          </Space>
          {newAccount.error && (
            <Typography className="error">{newAccount.error}</Typography>
          )}
        </div>
      )}
      {!newAccount.creating && (
        <Button type="primary" onClick={handleClickNewAccount}>
          Create account
        </Button>
      )}
    </div>
  );

  return (
    <Popover
      content={Content}
      placement="bottomRight"
      title={<Title level={5}>My Accounts</Title>}
      trigger="click"
      visible={open}
      onVisibleChange={handleVisibleChange}
    />
  );
};

export default AccountPanel;
